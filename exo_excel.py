import mysql.connector
from mysql.connector import Error, IntegrityError
import pandas as pd
import numpy as np

connection =  mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    database="fruits"
)

df = pd.read_excel('pivot-tables.xlsx', sheet_name='Sheet1')

# print(df.info())
# print(df.describe())
print(df.head(10))
# Order ID   Product    Category  Amount       Date         Country
# 0          1   Carrots  Vegetables    4270 2016-01-06   United States
# 1          2  Broccoli  Vegetables    8239 2016-01-07  United Kingdom
# 2          3    Banana       Fruit     617 2016-01-08   United States
df.set_index("Order ID", inplace=True)
df["Product"]= df["Product"].str.strip().apply(lambda x: x.lower())
df["Category"]= df["Category"].str.strip().apply(lambda x: x.lower())
df.loc[df["Category"] == "vegetables", "Category"] = "vegetable"
df["Country"]= df["Country"].str.strip().apply(lambda x: x.lower())
#print(df.head(10))

# df_product = df[["Product","Category"]].drop_duplicates()
# df_country = df["Country"].drop_duplicates() 

# ## INSERTS INTO DB
# cursor = connection.cursor()

# for index, row in df.iterrows():

#     name, category = row["Product"], row["Category"]
#     country = row["Country"]
#     amount, date = row["Amount"], row["Date"]
#     order_id = index

#     # Insert into table product
#     cursor.execute("""SELECT product_id FROM product WHERE name = %s;""", (name,))
#     result=cursor.fetchone()

#     if not result:
#         cursor.execute("""INSERT INTO product (name,category) VALUES (%s,%s);""", (name, category))
#         connection.commit()
#         product_id = cursor.lastrowid
           
#     else:
#         product_id = result[0]

#     # Insert into table country
#     cursor.execute("""SELECT country_id FROM country WHERE name = %s;""", (country,))
#     result=cursor.fetchone()

#     if not result:
#         cursor.execute("""INSERT INTO country (name) VALUES (%s);""", (country,))
#         connection.commit()
#         country_id = cursor.lastrowid
           
#     else:
#         country_id = result[0]

#     # Insert into relation table order
#     try:
#         sql = """INSERT INTO orders (order_id, product, amount, date, country) VALUES (%s, %s, %s, %s, %s);"""
#         cursor.execute(sql, (order_id, product_id, amount, date, country_id))
#         connection.commit()

#     except IntegrityError as e:
#         # Handling duplicate entry error
#         if e.errno == 1062:
#             print("Duplicate entry error. Skipping insertion of duplicate record.")
#         else:
#             print("Error occurred:", e)

# cursor.close()

################################################################################
######################### CREATE PIVOT TABLES WITH PANDAS ######################

pivot1 = pd.pivot_table(df, values="Amount", index=["Country","Product"], aggfunc="count", margins=True, margins_name="Totals")
print(pivot1)

pivot2 = pd.pivot_table(df, values="Amount", index="Country", columns="Product", aggfunc="sum", margins=True, margins_name="Totals")
print(pivot2)

######################### FUNCTIONS TO CREATE PIVOT TABLES #####################

cursor2=connection.cursor()
cursor2.execute("""SELECT DISTINCT name FROM country;""")
results = cursor2.fetchall()
print(results)
# output: [('united states',), ('united kingdom',), ('canada',), ('germany',), ('australia',), ('new zealand',), ('france',)]
# Convert the list of tuples into a list
countries = [result[0] for result in results]
print(countries)
# ['united states', 'united kingdom', 'canada', 'germany', 'australia', 'new zealand', 'france']

#placeholders = ', '.join(['%s'] * len(countries))       # %s, %s, %s, %s, %s, %s, %s

def count_product_by_country(country_list):
    # Construct the placeholder string with the number of placeholders
    placeholders = ', '.join(['%s'] * len(country_list))
    # Format the SQL query string with the placeholder string
    sql = f"""SELECT pr.name, COUNT(orders.order_id) AS Count 
             FROM product AS pr
             JOIN orders ON pr.product_id = orders.product
             JOIN country AS c ON orders.country = c.country_id
             WHERE c.name IN ({placeholders}) 
             GROUP BY pr.name;"""
    # Execute the query with each country as a separate parameter
    cursor2.execute(sql, country_list)
    result = cursor2.fetchall()
    df_pivot1 = pd.DataFrame(result)
    df_pivot1.rename(columns={0:"Product", 1:"Count"}, inplace=True)
    pivot1 = pd.pivot_table(df_pivot1, values="Count", index="Product", margins=True, margins_name="Totals")
    print(pivot1)

#print(count_product_by_country(countries))
list_eu=['germany']
print(count_product_by_country(list_eu))

def product_by_cat_country(cat_list, country_list):
    




cursor2.close()
connection.close()

