DROP DATABASE IF EXISTS fruits;
CREATE DATABASE fruits;
Use fruits;

CREATE TABLE `product` (
  `product_id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` text,
  `category` enum('fruit', 'vegetable')
);

CREATE TABLE `country` (
  `country_id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` text
);

CREATE TABLE `orders` (
  `order_id` INT PRIMARY KEY NOT NULL,
  `product` INT NOT NULL,
  `amount` INT NOT NULL,
  `date` DATE NOT NULL,
  `country` INT NOT NULL
);

ALTER TABLE `orders` ADD FOREIGN KEY (`country`) REFERENCES `country` (`country_id`);

ALTER TABLE `orders` ADD FOREIGN KEY (`product`) REFERENCES `product` (`product_id`);

GRANT ALL PRIVILEGES ON fruits.* TO 'lolam1'@'localhost';

